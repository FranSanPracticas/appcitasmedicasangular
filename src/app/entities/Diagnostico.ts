import { Cita } from "./Cita";

export class Diagnostico {

    private id:number;
    private enfermedad: String;
    private valoracionEspecialista: String;
    private cita: Cita;
}
