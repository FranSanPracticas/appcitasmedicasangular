import { Cita } from "./Cita";
import { Paciente } from "./Paciente";

export class Medico extends Usuario {
    numColegiado: String;
    citas: Cita[];
    pacientes: Paciente[];
}
