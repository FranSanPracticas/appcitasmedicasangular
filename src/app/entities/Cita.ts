import { Paciente } from "./Paciente";
import { Medico } from "./Medico";
import { Diagnostico } from "./Diagnostico";


export class Cita {
    id: number;
    fechaHora: Date;
    motivoCita: String;
    paciente: Paciente;
    medico: Medico;
    diagnostico: Diagnostico;
}
