import { Cita } from "./Cita";
import { Medico } from "./Medico";


export class Paciente extends Usuario {
    NSS: String;
    numTarjeta: String;
    telefono: String;
    direccion: String;
    citas: Cita[];
    medicos: Medico[];
}
